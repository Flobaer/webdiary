# Web Diary
## Install PostgreSQL
```bash
apt install postgresql libq-dev
```

Set up PostgreSQL by starting its interactive shell

```bash
sudo -u postgres psql
```

and create the necessary database and user configuration there:

```bash
CREATE USER django WITH PASSWORD 'oXbTlfMNOIS0niEQXxsfGJ4aqtk34lo2';
ALTER ROLE django SET client_encoding TO 'utf8';
ALTER ROLE django SET default_transaction_isolation TO 'read committed';
ALTER ROLE django SET timezone TO 'UTC';
ALTER USER django CREATEDB;

CREATE DATABASE webdiary;
GRANT ALL PRIVILEGES ON DATABASE webdiary TO django;
```

## Install Python dependencies
```bash
pip install -r requirements.txt
```
