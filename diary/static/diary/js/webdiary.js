"use strict";

function loadLocations() {
    var diaryOwner = document.getElementById("id_owner").value;
    var defaultOption = document.createElement("option");
    defaultOption.setAttribute("value", "");
    defaultOption.setAttribute("disabled", "");
    defaultOption.innerHTML = "-- Bitte erst Tagebuch auswählen --"

    if (diaryOwner === "") {
        var locationField = document.getElementById("id_locations");
        while (locationField.lastChild) {
            locationField.removeChild(locationField.lastChild);
        }

        locationField.appendChild(defaultOption);

    } else {
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var locations = JSON.parse(this.responseText);

                var locationField = document.getElementById("id_locations");
                while (locationField.lastChild) {
                    locationField.removeChild(locationField.lastChild);
                }

                var options = [];

                if (locations["locations"].length === 0) {
                    defaultOption.innerHTML = "-- Für dieses Tagebuch existieren keine Orte --"
                    options.push(defaultOption);
                } else {
                    for (var loc of locations["locations"]) {
                        var option = document.createElement("option");
                        option.value = loc[0];
                        option.innerHTML = loc[1];
                        options.push(option);
                    }
                }

                for (var option of options) {
                    locationField.appendChild(option);
                }
            }
        }

        xhttp.open("GET", "/tagebuch/api/locations/?diaryOwner=" + diaryOwner, true)
        xhttp.send()
    }
}

function loadTags() {
    var diaryOwner = document.getElementById("id_owner").value;
    var defaultOption = document.createElement("option");
    defaultOption.setAttribute("value", "");
    defaultOption.setAttribute("disabled", "");
    defaultOption.innerHTML = "-- Bitte erst Tagebuch auswählen --"

    if (diaryOwner === "") {
        var tagField = document.getElementById("id_tags");
        while (tagField.lastChild) {
            tagField.removeChild(tagField.lastChild);
        }

        tagField.appendChild(defaultOption);

    } else {
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var tags = JSON.parse(this.responseText);

                var tagField = document.getElementById("id_tags");
                while (tagField.lastChild) {
                    tagField.removeChild(tagField.lastChild);
                }

                var options = [];

                if (tags["tags"].length === 0) {
                    defaultOption.innerHTML = "-- Für dieses Tagebuch existieren keine Tags --"
                    options.push(defaultOption);
                } else {
                    for (var tag of tags["tags"]) {
                        var option = document.createElement("option");
                        option.value = tag[0];
                        option.innerHTML = tag[1];
                        options.push(option);
                    }
                }

                for (var option of options) {
                    tagField.appendChild(option);
                }
            }
        }

        xhttp.open("GET", "/tagebuch/api/tags/?diaryOwner=" + diaryOwner, true)
        xhttp.send()
    }
}

loadLocations();
loadTags();

document.getElementById("id_owner").addEventListener("click", loadLocations);
document.getElementById("id_owner").addEventListener("click", loadTags);
