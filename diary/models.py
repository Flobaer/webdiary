import random

from django.db import IntegrityError, models
from django.utils.text import slugify


def generate_unique_handle(length):
    alphabet = '134567890cfghjklrvwxy'
    primary_key = []

    for i in range(length):
        primary_key.append(random.choice(alphabet))

    return ''.join(primary_key)


class DiaryOwner(models.Model):
    handle = models.CharField(max_length=255, unique=True)
    first_name = models.CharField(max_length=255)
    middle_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255)
    birthday = models.DateField()
    time_of_birth = models.TimeField(blank=True, null=True)
    place_of_birth = models.ForeignKey('Location', on_delete=models.SET_NULL,
                                       blank=True, null=True)

    def str_short(self):
        return "{}{}{}".format(self.first_name[0], self.middle_name[0:1],
                               self.last_name[0])

    def __str__(self):
        return "{} {} {}".format(self.first_name, self.middle_name,
                                 self.last_name)

    def save(self, *args, **kwargs):
        handle_length = 4
        if not self.handle:
            self.handle = generate_unique_handle(handle_length)

        success = False
        failures = 0
        while not success:
            try:
                super().save(*args, **kwargs)
            except IntegrityError:
                failures += 1
                if failures > 1000:
                    handle_length += 1
                    failures = 0
                else:
                    self.handle = generate_unique_handle(handle_length)
            else:
                success = True

    class Meta:
        verbose_name_plural = "Diary Owners"


class DiaryEntry(models.Model):
    owner = models.ForeignKey('DiaryOwner', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    date = models.DateField()
    content = models.TextField()
    tags = models.ManyToManyField('Tag', blank=True)
    locations = models.ManyToManyField('Location', blank=True)
    creation_time = models.DateTimeField()

    def weekday(self):
        weekday_mapping = {
            0: "Montag",
            1: "Dienstag",
            2: "Mittwoch",
            3: "Donnerstag",
            4: "Freitag",
            5: "Samstag",
            6: "Sonntag",
        }

        return weekday_mapping[self.date.weekday()]

    def __str__(self):
        return "{}, {}: {}".format(self.date.strftime('%d.%m.%Y'),
                                   self.weekday(), self.title)

    class Meta:
        ordering = ['date']
        verbose_name_plural = "Diary Entries"
        unique_together = ('owner', 'date')


class Location(models.Model):
    name = models.CharField(unique=True, max_length=255)
    owner = models.ForeignKey('DiaryOwner', on_delete=models.CASCADE)
    slug = models.SlugField(unique=True)
    latitude = models.CharField(max_length=255, blank=True)
    longitude = models.CharField(max_length=255, blank=True)
    postal_code = models.CharField(max_length=255, blank=True)
    street = models.CharField(max_length=255, blank=True)
    house_number = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        self.slug = self.slug or slugify(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.name)


class Tag(models.Model):
    TAG_TYPES = (
        ('', '---'),
        ('event', 'Ereignis'),
        ('institution', 'Institution'),
        ('person', 'Person'),
    )

    name = models.CharField(unique=True, max_length=255)
    owner = models.ForeignKey('DiaryOwner', on_delete=models.CASCADE)
    type_of_tag = models.CharField(max_length=255, choices=TAG_TYPES,
                                   blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return "#{}".format(self.name)


class Asset(models.Model):
    entry = models.ForeignKey('DiaryEntry', on_delete=models.CASCADE)
    asset = models.FileField(blank=True, null=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return "{}".format(self.description)
