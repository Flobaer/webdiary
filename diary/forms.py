from django import forms
from tinymce.widgets import TinyMCE

from .models import DiaryEntry, DiaryOwner, Location, Tag


class CreateDiaryOwnerForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = DiaryOwner
        exclude = ['handle', 'place_of_birth', 'time_of_birth']
        labels = {
            'first_name': 'Vorname',
            'middle_name': 'Zweite(r) Vorname(n)',
            'last_name': 'Nachname',
            'birthday': 'Geburtsdatum',
        }


class DiaryEntryForm(forms.ModelForm):
    required_css_class = 'required'

    content = forms.CharField(label='Eintrag', widget=TinyMCE)
    owner = forms.ModelChoiceField(DiaryOwner.objects.all(),
                                   to_field_name='handle',
                                   label='Tagebuch von',)

    class Meta:
        model = DiaryEntry
        exclude = ['creation_time']
        fields = [
            'owner',
            'title',
            'date',
            'locations',
            'tags',
            'content',
        ]
        labels = {
            'title': 'Titel',
            'date': 'Datum',
            'locations': 'Ort(e)',
            'tags': 'Tag(s)',
        }

    def clean(self):
        cleaned_data = super().clean()

        owner = cleaned_data.get('owner')
        locations = cleaned_data.get('locations')
        tags = cleaned_data.get('tags')

        if locations:
            for location in locations:
                if location.owner != owner:
                    error_message = ("Der angegebene Ort {} gehört nicht zum "
                                     "Tagebuch von {} sondern zum Tagebuch "
                                     "von {}.").format(location, owner,
                                                       location.owner)
                    raise forms.ValidationError(error_message)

        if tags:
            for tag in tags:
                if tag.owner != owner:
                    error_message = ("Das angegebene Tag {} gehört nicht zum "
                                     "Tagebuch von {} sondern zum Tagebuch "
                                     "von {}.").format(tag, owner, tag.owner)
                    raise forms.ValidationError(error_message)


class LocationForm(forms.ModelForm):
    required_css_class = 'required'

    owner = forms.ModelChoiceField(DiaryOwner.objects.all(),
                                   to_field_name='handle',
                                   label='Tagebuch von',)

    class Meta:
        model = Location
        exclude = ['slug']
        labels = {
            'name': 'Name',
            'latitude': 'Breitengrad',
            'longitude': 'Längengrad',
            'postal_code': 'PLZ',
            'street': 'Straße',
            'house_number': 'Hausnummer',
            'city': 'Ort',
            'description': 'Beschreibung',
        }


class TagForm(forms.ModelForm):
    required_css_class = 'required'

    owner = forms.ModelChoiceField(DiaryOwner.objects.all(),
                                   to_field_name='handle',
                                   label='Tagebuch von',)

    class Meta:
        model = Tag
        fields = ['name', 'owner', 'type_of_tag', 'description']
        labels = {
            'name': 'Name',
            'type_of_tag': 'Art des Tags',
            'description': 'Beschreibung',
        }

    def clean_name(self):
        name = self.cleaned_data.get('name')

        if any(character.isspace() for character in name):
            raise forms.ValidationError(
                "Der Name des Tags darf keine Leerzeichen, Tabulator-Zeichen, "
                "Zeilenumbrüche oder Ähnliches enthalten."
            )

        return name
