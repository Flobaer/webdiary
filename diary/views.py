import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse

from .forms import CreateDiaryOwnerForm, DiaryEntryForm, LocationForm, TagForm
from .models import DiaryEntry, DiaryOwner, Location, Tag


@login_required
def get_locations(request):
    queried_owner_handle = request.GET.get('diaryOwner', '')
    locations = []

    if queried_owner_handle:
        diary_owner = get_object_or_404(DiaryOwner,
                                        handle=queried_owner_handle)
        for location in Location.objects.filter(owner=diary_owner):
            locations.append((location.id, location.name))

    return JsonResponse({'locations': locations})


@login_required
def get_tags(request):
    queried_owner_handle = request.GET.get('diaryOwner', '')
    tags = []

    if queried_owner_handle:
        diary_owner = get_object_or_404(DiaryOwner,
                                        handle=queried_owner_handle)
        for tag in Tag.objects.filter(owner=diary_owner):
            tags.append((tag.id, tag.name))

    return JsonResponse({'tags': tags})


@login_required
def recent_changes_view(request):
    entries = DiaryEntry.objects.order_by('-creation_time')
    context = {'entries': entries}
    return render(request, 'diary/recent_changes.html', context)


@login_required
def new_diary_view(request):
    if request.method == 'POST':
        form = CreateDiaryOwnerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('diary:index')
    else:
        form = CreateDiaryOwnerForm()

    return render(request, 'diary/new_diary.html', {'form': form})


@login_required
def new_entry_view(request):
    if request.method == 'POST':
        form = DiaryEntryForm(request.POST)
        if form.is_valid():
            entry = form.save(commit=False)
            entry.creation_time = datetime.datetime.now()
            entry.save()
            form.save_m2m()

            return HttpResponseRedirect(reverse('diary:entry_detail',
                                        args=[entry.owner.handle, entry.date]))
    else:
        form = DiaryEntryForm()

    return render(request, 'diary/new_entry.html', {'form': form})


@login_required
def new_location_view(request):
    if request.method == 'POST':
        form = LocationForm(request.POST)
        if form.is_valid():
            location = form.save()

            return HttpResponseRedirect(reverse('diary:location_detail',
                                        args=[location.slug]))
    else:
        form = LocationForm()

    return render(request, 'diary/new_location.html', {'form': form})


@login_required
def new_tag_view(request):
    if request.method == 'POST':
        form = TagForm(request.POST)
        if form.is_valid():
            tag = form.save()

            return HttpResponseRedirect(reverse('diary:tag_detail',
                                        args=[tag.name]))
    else:
        form = TagForm()

    return render(request, 'diary/new_tag.html', {'form': form})


@login_required
def edit_owner_view(request, handle):
    owner = get_object_or_404(DiaryOwner, handle=handle)
    if request.method == 'POST':
        form = CreateDiaryOwnerForm(request.POST, instance=owner)
        if form.is_valid():
            owner = form.save()

            return HttpResponseRedirect(reverse('diary:owner_detail',
                                        args=[owner.handle]))
    else:
        form = CreateDiaryOwnerForm(instance=owner)

    return render(request, 'diary/new_diary.html', {'form': form})


@login_required
def delete_owner_view(request, handle):
    owner = get_object_or_404(DiaryOwner, handle=handle)
    owner.delete()

    return redirect('diary:owner_list')


@login_required
def edit_entry_view(request, handle, date):
    entry = get_object_or_404(DiaryEntry, date=date, owner__handle=handle)
    owner = get_object_or_404(DiaryOwner, handle=handle)
    if request.method == 'POST':
        form = DiaryEntryForm(request.POST, instance=entry)
        if form.is_valid():
            entry = form.save()

            return HttpResponseRedirect(reverse('diary:entry_detail',
                                        args=[entry.owner.handle, entry.date]))
    else:
        form = DiaryEntryForm(instance=entry, initial={'owner': owner.handle})

    return render(request, 'diary/new_entry.html', {'form': form})


@login_required
def delete_entry_view(request, handle, date):
    entry = get_object_or_404(DiaryEntry, date=date, owner__handle=handle)
    entry.delete()

    return redirect('diary:entry_list')


@login_required
def edit_location_view(request, slug):
    location = get_object_or_404(Location, slug=slug)
    if request.method == 'POST':
        form = LocationForm(request.POST, instance=location)
        if form.is_valid():
            location = form.save()

            return HttpResponseRedirect(reverse('diary:location_detail',
                                        args=[location.slug]))
    else:
        form = LocationForm(instance=location)

    return render(request, 'diary/new_location.html', {'form': form})


@login_required
def delete_location_view(request, name):
    location = get_object_or_404(Location, slug=name)
    location.delete()

    return redirect('diary:location_list')


@login_required
def edit_tag_view(request, name):
    tag = get_object_or_404(Tag, name=name)
    if request.method == 'POST':
        form = TagForm(request.POST, instance=tag)
        if form.is_valid():
            tag = form.save()

            return HttpResponseRedirect(reverse('diary:tag_detail',
                                        args=[tag.name]))
    else:
        form = TagForm(instance=tag)

    return render(request, 'diary/new_tag.html', {'form': form})


@login_required
def delete_tag_view(request, name):
    tag = get_object_or_404(Tag, name=name)
    tag.delete()

    return redirect('diary:tag_list')


@login_required
def owner_list_view(request):
    owners = DiaryOwner.objects.all()
    context = {'owners': owners}

    return render(request, 'diary/owner_list.html', context)


@login_required
def entry_list_view(request):
    entries = DiaryEntry.objects.all()
    context = {'entries': entries}

    return render(request, 'diary/entry_list.html', context)


@login_required
def location_list_view(request):
    locations = Location.objects.all()
    context = {'locations': locations}

    return render(request, 'diary/location_list.html', context)


@login_required
def tag_list_view(request):
    tags = Tag.objects.all()
    context = {'tags': tags}

    return render(request, 'diary/tag_list.html', context)


@login_required
def owner_detail_view(request, handle):
    owner = get_object_or_404(DiaryOwner, handle=handle)
    entries = DiaryEntry.objects.filter(owner__handle=handle)
    context = {'owner': owner, 'entries': entries}

    return render(request, 'diary/owner_detail.html', context)


@login_required
def entry_detail_view(request, handle, date):
    entry = get_object_or_404(DiaryEntry, date=date, owner__handle=handle)
    context = {'entry': entry}

    return render(request, 'diary/entry_detail.html', context)


@login_required
def location_detail_view(request, slug):
    location = get_object_or_404(Location, slug=slug)
    context = {'location': location}

    return render(request, 'diary/location_detail.html', context)


@login_required
def tag_detail_view(request, name):
    tag = get_object_or_404(Tag, name=name)
    context = {'tag': tag}

    return render(request, 'diary/tag_detail.html', context)
