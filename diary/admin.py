from django.contrib import admin

from .models import DiaryEntry, DiaryOwner, Asset, Tag, Location

admin.site.register(DiaryEntry)
admin.site.register(DiaryOwner)
admin.site.register(Asset)
admin.site.register(Tag)
admin.site.register(Location)
