import datetime as dt

from django.test import SimpleTestCase, TestCase
from diary.forms import (CreateDiaryOwnerForm, DiaryEntryForm, LocationForm,
                         TagForm)
from diary.models import DiaryOwner, Location, Tag


class CreateDiaryOwnerFormTest(SimpleTestCase):
    def test_insufficient_input(self):
        first_name = 'test_name_fn'
        last_name = 'test_name_sn'
        birthday = dt.date(2000, 1, 1)

        empty_form = CreateDiaryOwnerForm()

        sparse_form_1_of_3_1 = CreateDiaryOwnerForm(
            data={'first_name': first_name})
        sparse_form_1_of_3_2 = CreateDiaryOwnerForm(
            data={'last_name': last_name})
        sparse_form_1_of_3_3 = CreateDiaryOwnerForm(
            data={'birthday': birthday})

        sparse_form_2_of_3_1 = CreateDiaryOwnerForm(
            data={'first_name': first_name, 'last_name': last_name})
        sparse_form_2_of_3_2 = CreateDiaryOwnerForm(
            data={'first_name': first_name, 'birthday': birthday})
        sparse_form_2_of_3_3 = CreateDiaryOwnerForm(
            data={'last_name': last_name, 'birthday': birthday})

        self.assertFalse(empty_form.is_valid())

        self.assertFalse(sparse_form_1_of_3_1.is_valid())
        self.assertFalse(sparse_form_1_of_3_2.is_valid())
        self.assertFalse(sparse_form_1_of_3_3.is_valid())

        self.assertFalse(sparse_form_2_of_3_1.is_valid())
        self.assertFalse(sparse_form_2_of_3_2.is_valid())
        self.assertFalse(sparse_form_2_of_3_3.is_valid())

    def test_minimum_input(self):
        first_name = 'test_name_fn'
        last_name = 'test_name_sn'
        birthday = dt.date(2000, 1, 1)

        minimum_form = CreateDiaryOwnerForm(data={'first_name': first_name,
                                                  'last_name': last_name,
                                                  'birthday': birthday})

        self.assertTrue(minimum_form.is_valid())

    def test_complete_input(self):
        first_name = 'test_name_fn'
        middle_name = 'test_name_mn'
        last_name = 'test_name_sn'
        birthday = dt.date(2000, 1, 1)
        time_of_birth = dt.time(20, 15, 0)

        complete_form = CreateDiaryOwnerForm(data={
            'first_name': first_name, 'middle_name': middle_name,
            'last_name': last_name, 'birthday': birthday,
            'time_of_birth': time_of_birth
        })

        self.assertTrue(complete_form.is_valid())

    def test_empty_string_not_valid(self):
        first_name = 'test_name_fn'
        last_name = 'test_name_sn'
        birthday = dt.date(2000, 1, 1)

        empty_first_name_form = CreateDiaryOwnerForm(
            data={'first_name': '', 'last_name': last_name,
                  'birthday': birthday}
        )
        empty_last_name_form = CreateDiaryOwnerForm(
            data={'first_name': first_name, 'last_name': '',
                  'birthday': birthday}
        )
        empty_birthday_form = CreateDiaryOwnerForm(
            data={'first_name': first_name, 'last_name': last_name,
                  'birthday': ''}
        )

        self.assertFalse(empty_first_name_form.is_valid())
        self.assertFalse(empty_last_name_form.is_valid())
        self.assertFalse(empty_birthday_form.is_valid())


class DiaryEntryFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        owner = DiaryOwner.objects.create(first_name='test_name_fn',
                                          last_name='test_name_sn',
                                          birthday=dt.date(2000, 1, 1))
        Location.objects.create(name='test_location_1', owner=owner)
        Location.objects.create(name='test_location_2', owner=owner)
        Tag.objects.create(name='test_tag_1', owner=owner)
        Tag.objects.create(name='test_tag_2', owner=owner)

        owner_2 = DiaryOwner.objects.create(first_name='test_name_2_fn',
                                            last_name='test_name_2_sn',
                                            birthday=dt.date(2000, 2, 2))
        Location.objects.create(name='test_location_3', owner=owner_2)
        Tag.objects.create(name='test_tag_4', owner=owner_2)

    def test_insufficient_input(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        title = 'test title'
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        empty_form = DiaryEntryForm()
        sparse_form_1_of_4_1 = DiaryEntryForm(data={'owner': owner_handle})
        sparse_form_1_of_4_2 = DiaryEntryForm(data={'title': title})
        sparse_form_1_of_4_3 = DiaryEntryForm(data={'date': date})
        sparse_form_1_of_4_4 = DiaryEntryForm(data={'content': content})

        sparse_form_2_of_4_1 = DiaryEntryForm(data={'owner': owner_handle,
                                                    'title': title})
        sparse_form_2_of_4_2 = DiaryEntryForm(data={'owner': owner_handle,
                                                    'date': date})
        sparse_form_2_of_4_3 = DiaryEntryForm(data={'owner': owner_handle,
                                                    'content': content})
        sparse_form_2_of_4_4 = DiaryEntryForm(data={'title': title,
                                                    'date': date})
        sparse_form_2_of_4_5 = DiaryEntryForm(data={'title': title,
                                                    'content': content})
        sparse_form_2_of_4_6 = DiaryEntryForm(data={'date': date,
                                                    'content': content})

        sparse_form_3_of_4_1 = DiaryEntryForm(data={'owner': owner_handle,
                                                    'title': title,
                                                    'date': date})
        sparse_form_3_of_4_2 = DiaryEntryForm(data={'owner': owner_handle,
                                                    'title': title,
                                                    'content': content})
        sparse_form_3_of_4_3 = DiaryEntryForm(data={'owner': owner_handle,
                                                    'date': date,
                                                    'content': content})
        sparse_form_3_of_4_4 = DiaryEntryForm(data={'title': title,
                                                    'date': date,
                                                    'content': content})

        self.assertFalse(empty_form.is_valid())
        self.assertFalse(sparse_form_1_of_4_1.is_valid())
        self.assertFalse(sparse_form_1_of_4_2.is_valid())
        self.assertFalse(sparse_form_1_of_4_3.is_valid())
        self.assertFalse(sparse_form_1_of_4_4.is_valid())

        self.assertFalse(sparse_form_2_of_4_1.is_valid())
        self.assertFalse(sparse_form_2_of_4_2.is_valid())
        self.assertFalse(sparse_form_2_of_4_3.is_valid())
        self.assertFalse(sparse_form_2_of_4_4.is_valid())
        self.assertFalse(sparse_form_2_of_4_5.is_valid())
        self.assertFalse(sparse_form_2_of_4_6.is_valid())

        self.assertFalse(sparse_form_3_of_4_1.is_valid())
        self.assertFalse(sparse_form_3_of_4_2.is_valid())
        self.assertFalse(sparse_form_3_of_4_3.is_valid())
        self.assertFalse(sparse_form_3_of_4_4.is_valid())

    def test_minimum_input(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        title = 'test title'
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        minimum_form = DiaryEntryForm(data={
            'owner': owner_handle, 'title': title, 'date': date,
            'content': content
        })

        self.assertTrue(minimum_form.is_valid())

    def test_complete_input(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        title = 'test title'
        location = Location.objects.get(name='test_location_1').id
        tag = Tag.objects.get(name='test_tag_1').id
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        complete_form = DiaryEntryForm(data={
            'owner': owner_handle, 'title': title, 'date': date,
            'tags': [tag], 'locations': [location], 'content': content,
        })

        self.assertTrue(complete_form.is_valid())

    def test_multiple_tags(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        title = 'test title'
        location = Location.objects.get(name='test_location_1').id
        tag_1 = Tag.objects.get(name='test_tag_1')
        tag_2 = Tag.objects.get(name='test_tag_2')
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        complete_form = DiaryEntryForm(data={
            'owner': owner_handle,
            'title': title,
            'date': date,
            'tags': [location],
            'locations': [tag_1, tag_2],
            'content': content,
        })

        self.assertTrue(complete_form.is_valid())

    def test_multiple_locations(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        title = 'test title'
        location_1 = Location.objects.get(name='test_location_1').id
        location_2 = Location.objects.get(name='test_location_2').id
        tag = Tag.objects.get(name='test_tag_1')
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        complete_form = DiaryEntryForm(data={
            'owner': owner_handle,
            'title': title,
            'date': date,
            'tags': [location_1, location_2],
            'locations': [tag],
            'content': content,
        })

        self.assertTrue(complete_form.is_valid())

    def test_multiple_tags_and_locations(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        title = 'test title'
        location_1 = Location.objects.get(name='test_location_1').id
        location_2 = Location.objects.get(name='test_location_2').id
        tag_1 = Tag.objects.get(name='test_tag_1')
        tag_2 = Tag.objects.get(name='test_tag_2')
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        complete_form = DiaryEntryForm(data={
            'owner': owner_handle,
            'title': title,
            'date': date,
            'tags': [location_1, location_2],
            'locations': [tag_1, tag_2],
            'content': content,
        })

        self.assertTrue(complete_form.is_valid())

    def test_empty_string_not_valid(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        title = 'test title'
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        empty_owner_form = DiaryEntryForm(data={'owner': '',
                                                'title': title,
                                                'date': date,
                                                'content': content})
        empty_title_form = DiaryEntryForm(data={'owner': owner_handle,
                                                'title': '',
                                                'date': date,
                                                'content': content})
        empty_date_form = DiaryEntryForm(data={'owner': owner_handle,
                                               'title': title,
                                               'date': '',
                                               'content': content})
        empty_content_form = DiaryEntryForm(data={'owner': owner_handle,
                                                  'title': title,
                                                  'date': date,
                                                  'content': ''})

        self.assertFalse(empty_owner_form.is_valid())
        self.assertFalse(empty_title_form.is_valid())
        self.assertFalse(empty_date_form.is_valid())
        self.assertFalse(empty_content_form.is_valid())

    def test_tags_must_belong_to_the_same_diary_owner(self):
        owner_handle = DiaryOwner.objects.get(
            first_name='test_name_2_fn').handle

        title = 'test title'
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        form = DiaryEntryForm(data={
            'owner': owner_handle, 'title': title, 'date': date,
            'content': content,
            'tags': [Tag.objects.get(name='test_tag_2').id]
        })

        self.assertFalse(form.is_valid())

    def test_locations_must_belong_to_the_same_diary_owner(self):
        owner_handle = DiaryOwner.objects.get(
            first_name='test_name_2_fn').handle

        title = 'test title'
        date = dt.date(2000, 1, 1,)
        content = 'test content'

        form = DiaryEntryForm(data={
            'owner': owner_handle, 'title': title, 'date': date,
            'content': content,
            'locations': [Location.objects.get(name='test_location_2').id]
        })

        self.assertFalse(form.is_valid())


class LocationFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        DiaryOwner.objects.create(first_name='test_name_fn',
                                  last_name='test_name_sn',
                                  birthday=dt.date(2000, 1, 1))

    def test_insufficient_input(self):
        empty_form = LocationForm()
        self.assertFalse(empty_form.is_valid())

    def test_minimum_input(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        minimum_form = LocationForm(data={'name': 'test_location',
                                          'owner': owner_handle})
        self.assertTrue(minimum_form.is_valid())

    def test_complete_input(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        complete_form = LocationForm(data={
            'name': 'test_location', 'owner': owner_handle,
            'latitude': '12345', 'longitude': '67890', 'postal_code': '54321',
            'street': 'test_street', 'house_number': '12', 'city': 'test_city',
            'description': 'test description'
        })

        self.assertTrue(complete_form.is_valid())

    def test_empty_string_not_valid(self):
        empty_name_form = LocationForm(data={'name': ''})
        self.assertFalse(empty_name_form.is_valid())


class TagFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        DiaryOwner.objects.create(first_name='test_name_fn',
                                  last_name='test_name_sn',
                                  birthday=dt.date(2000, 1, 1))

    def test_insufficient_input(self):
        empty_form = TagForm()
        self.assertFalse(empty_form.is_valid())

    def test_minimum_input(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        minimum_form = TagForm(data={'name': 'test_tag',
                                     'owner': owner_handle})
        self.assertTrue(minimum_form.is_valid())

    def test_complete_input(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        complete_form = TagForm(data={
            'name': 'test_location', 'owner': owner_handle,
            'type_of_tag': 'event', 'description': 'test description'
        })

        self.assertTrue(complete_form.is_valid())

    def test_empty_string_not_valid(self):
        empty_name_form = TagForm(data={'name': ''})
        self.assertFalse(empty_name_form.is_valid())

    def test_valid_types_of_tags(self):
        owner_handle = DiaryOwner.objects.get(first_name='test_name_fn').handle
        for combination in Tag.TAG_TYPES:
            form = TagForm(data={'name': 'test_tag', 'owner': owner_handle,
                                 'type_of_tag': combination[0]})
            self.assertTrue(form.is_valid())

    def test_invalid_type_of_tag(self):
        form = TagForm(data={'name': 'test_tag',
                             'type_of_tag': 'random invalid type of tag'})
        self.assertFalse(form.is_valid())

    def test_whitespace_in_tag_name(self):
        test_cases = [
            'center space', 'center\ttab', 'center\nnewline'
        ]

        for test_case in test_cases:
            form = TagForm(data={'name': test_case})
            self.assertFalse(form.is_valid())
