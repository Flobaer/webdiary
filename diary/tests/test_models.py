import datetime as dt

from django.db.utils import IntegrityError
from django.test import TestCase
from django.utils.timezone import make_aware

from diary.models import (DiaryEntry, DiaryOwner, generate_unique_handle,
                          Location, Tag)


class DiaryOwnerModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        location = Location.objects.create(name='test_location', owner=owner)
        tag = Tag.objects.create(name='test_tag', owner=owner)
        entry = DiaryEntry.objects.create(
            owner=owner, date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime.now())
        )
        entry.locations.add(location)
        entry.tags.add(tag)

    def test_unique_handle_scheme(self):
        alphabet = set('134567890cfghjklrvwxy')

        for _ in range(100000):
            primary_key = generate_unique_handle(4)
            self.assertTrue(len(primary_key) == 4)
            self.assertTrue(set(primary_key) <= alphabet)

        diary_owner = DiaryOwner.objects.get(first_name='test_owner_fn')

        self.assertTrue(len(diary_owner.handle) == 4)
        self.assertTrue(set(diary_owner.handle) <= set(alphabet))

    def test_on_deletion_also_delete_entries(self):
        DiaryOwner.objects.get(first_name='test_owner_fn').delete()
        entries = DiaryEntry.objects.filter(owner__first_name='test_owner_fn')
        self.assertEqual(len(entries), 0)

    def test_on_deletion_also_delete_locations(self):
        DiaryOwner.objects.get(first_name='test_owner_fn').delete()
        locations = Location.objects.filter(owner__first_name='test_owner_fn')
        self.assertEqual(len(locations), 0)

    def test_on_deletion_also_delete_tags(self):
        DiaryOwner.objects.get(first_name='test_owner_fn').delete()
        tags = Tag.objects.filter(owner__first_name='test_owner_fn')
        self.assertEqual(len(tags), 0)


class DiaryEntryModelTest(TestCase):
    @classmethod
    def setUp(self):
        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        location = Location.objects.create(name='test_location', owner=owner)
        tag = Tag.objects.create(name='test_tag', owner=owner)
        entry = DiaryEntry.objects.create(
            owner=owner, date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime.now())
        )
        entry.locations.add(location)
        entry.tags.add(tag)

    def test_weekday_mapping(self):
        dates = [
            (2019, 8, 19), (2018, 10, 23), (2017, 4, 26), (2016, 12, 15),
            (2015, 6, 26), (2014, 1, 18), (2013, 9, 29)
        ]

        expected_weekdays = [
            'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag',
            'Samstag', 'Sonntag'
        ]

        actual_weekdays = []

        for date in dates:
            diary_entry = DiaryEntry.objects.create(
                owner=DiaryOwner.objects.get(first_name='test_owner_fn'),
                date=dt.date(*date),
                creation_time=make_aware(dt.datetime.now())
            )
            actual_weekdays.append(diary_entry.weekday())

        self.assertEqual(actual_weekdays, expected_weekdays)

    def test_unique_owner_date_combination(self):
        existing_entry = DiaryEntry.objects.get(date=dt.date(2000, 1, 1))

        self.assertRaisesMessage(
            IntegrityError, 'duplicate key value violates unique constraint',
            DiaryEntry.objects.create,
            owner=existing_entry.owner, date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime.now()))

    def test_owner_not_null(self):
        self.assertRaisesMessage(
            IntegrityError, 'violates not-null constraint',
            DiaryEntry.objects.create,
            date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime.now())
        )

    def test_date_not_null(self):
        self.assertRaisesMessage(
            IntegrityError, 'violates not-null constraint',
            DiaryEntry.objects.create,
            owner=DiaryOwner.objects.get(first_name='test_owner_fn'),
            creation_time=make_aware(dt.datetime.now())
        )

    def test_creation_time_not_null(self):
        self.assertRaisesMessage(
            IntegrityError, 'violates not-null constraint',
            DiaryEntry.objects.create,
            owner=DiaryOwner.objects.get(first_name='test_owner_fn'),
            date=dt.date(2000, 1, 1),
        )

    def test_deletion_does_not_affect_diary_owner(self):
        DiaryEntry.objects.get(date=dt.date(2000, 1, 1)).delete()
        diary_owner = DiaryOwner.objects.filter(first_name='test_owner_fn')
        self.assertEqual(len(diary_owner), 1)

    def test_deletion_does_not_affect_locations(self):
        DiaryEntry.objects.get(date=dt.date(2000, 1, 1)).delete()
        location = Location.objects.filter(name='test_location')
        self.assertEqual(len(location), 1)

    def test_deletion_does_not_affect_tags(self):
        DiaryEntry.objects.get(date=dt.date(2000, 1, 1)).delete()
        tag = Tag.objects.filter(name='test_tag')
        self.assertEqual(len(tag), 1)


class LocationModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        location = Location.objects.create(name='test_location', owner=owner)
        location_2 = Location.objects.create(name='test_location_2',
                                             owner=owner)
        owner.place_of_birth = location
        diary_entry = DiaryEntry.objects.create(
            owner=owner, date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime.now())
        )
        diary_entry.locations.add(location_2)

    def test_unique_name(self):
        self.assertRaisesMessage(
            IntegrityError, 'duplicate key value violates unique constraint',
            Location.objects.create,
            name=Location.objects.get(name='test_location').name,
            owner=Location.objects.get(name='test_location').owner
        )

    def test_deletion_does_not_affect_diary_owner(self):
        Location.objects.get(name='test_location').delete()
        owners = DiaryOwner.objects.filter(first_name='test_owner_fn')
        self.assertEqual(len(owners), 1)

    def test_deletion_does_not_affect_diary_entries(self):
        Location.objects.get(name='test_location_2').delete()
        entries = DiaryEntry.objects.filter(date=dt.date(2000, 1, 1))
        self.assertEqual(len(entries), 1)


class TagModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        Tag.objects.create(name='test_tag', owner=owner)
        DiaryEntry.objects.create(
            owner=owner, date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime.now())
        )

    def test_unique_name(self):
        self.assertRaisesMessage(
            IntegrityError, 'duplicate key value violates unique constraint',
            Tag.objects.create,
            name=Tag.objects.get(name='test_tag').name,
            owner=Tag.objects.get(name='test_tag').owner
        )

    def test_deletion_does_not_affect_diary_owner(self):
        Tag.objects.get(name='test_tag').delete()
        owners = DiaryOwner.objects.filter(first_name='test_owner_fn')
        self.assertEqual(len(owners), 1)

    def test_deletion_does_not_affect_diary_entries(self):
        Tag.objects.get(name='test_tag').delete()
        entries = DiaryEntry.objects.filter(date=dt.date(2000, 1, 1))
        self.assertEqual(len(entries), 1)
