import datetime as dt

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils.text import slugify
from django.utils.timezone import make_aware

from diary.forms import (CreateDiaryOwnerForm, DiaryEntryForm, LocationForm,
                         TagForm)
from diary.models import DiaryEntry, DiaryOwner, Location, Tag


class NewDiaryViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:new_diary'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/neues_tagebuch/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_diary'))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_diary'))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'],
                                   CreateDiaryOwnerForm))


class NewEntryViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:new_entry'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/neuer_eintrag/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_entry'))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_entry'))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'], DiaryEntryForm))


class NewLocationViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:new_location'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/neuer_ort/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_location'))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_location'))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'], LocationForm))


class NewTagViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:new_tag'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/neues_tag/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_tag'))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:new_tag'))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'], TagForm))


class EditOwnerViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)  # , place_of_birth=location
        )
        location = Location.objects.create(name='test_location', owner=owner)
        owner.place_of_birth = location
        owner.save()

    def test_view_redirect_to_login(self):
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse('diary:edit_owner',
                                           args=[handle]))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get('/besitzer/{}/bearbeiten/'.format(handle))
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse('diary:edit_owner',
                                           args=[handle]))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse('diary:edit_owner',
                                           args=[handle]))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'],
                                   CreateDiaryOwnerForm))

    def test_form_contains_prefilled_values(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse('diary:edit_owner',
                                           args=[owner.handle]))
        self.assertEqual(response.context['form'].initial['first_name'],
                         'test_owner_fn')
        self.assertEqual(response.context['form'].initial['middle_name'],
                         'test_owner_mn')
        self.assertEqual(response.context['form'].initial['last_name'],
                         'test_owner_sn')
        self.assertEqual(response.context['form'].initial['birthday'],
                         dt.date(2000, 1, 1))


class DeleteOwnerViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def setUp(self):
        DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )

    def test_view_redirect_to_login(self):
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse('diary:delete_owner',
                                           args=[handle]))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get('/besitzer/{}/loeschen/'.format(handle))
        self.assertRedirects(response, reverse('diary:owner_list'))

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse('diary:delete_owner',
                                           args=[handle]))
        self.assertRedirects(response, reverse('diary:owner_list'))

    def test_show_404_if_owner_does_not_exist(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:delete_owner',
                                           args=['vwxy']))
        self.assertEqual(response.status_code, 404)

    def test_entry_is_deleted_from_database(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse('diary:delete_owner',
                                           args=[handle]))
        self.assertRaisesMessage(
            DiaryOwner.DoesNotExist,
            'DiaryOwner matching query does not exist',
            DiaryOwner.objects.get, handle=handle,
        )
        self.assertRedirects(response, reverse('diary:owner_list'))


class EditEntryViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        entry = DiaryEntry.objects.create(
            owner=owner, title='test_title', date=dt.date(2000, 1, 1),
            content='test content',
            creation_time=make_aware(dt.datetime(2000, 1, 1, 21, 0, 0))
        )

        location = Location.objects.create(name='test_location', owner=owner)
        tag = Tag.objects.create(name='test_tag', owner=owner)

        owner.place_of_birth = location
        entry.tags.add(tag)
        entry.locations.add(location)

    def test_view_redirect_to_login(self):
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse(
            'diary:edit_entry',
            args=[handle, dt.date(2000, 1, 1)]
        ))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get('/{}/{}/bearbeiten/'.format(
            handle, dt.date(2000, 1, 1))
        )
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse(
            'diary:edit_entry',
            args=[handle, make_aware(dt.datetime(2000, 1, 1))]
        ))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        handle = DiaryOwner.objects.get(first_name='test_owner_fn').handle
        response = self.client.get(reverse(
            'diary:edit_entry',
            args=[handle, make_aware(dt.datetime(2000, 1, 1))]
        ))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'], DiaryEntryForm))

    def test_form_contains_prefilled_values(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        entry = DiaryEntry.objects.get(owner__handle=owner.handle,
                                       date=dt.date(2000, 1, 1))
        response = self.client.get(reverse(
            'diary:edit_entry',
            args=[owner.handle, make_aware(dt.datetime(2000, 1, 1))]
        ))
        self.assertEqual(response.context['form'].initial['owner'],
                         owner.handle)
        self.assertEqual(response.context['form'].initial['title'],
                         'test_title')
        self.assertEqual(response.context['form'].initial['date'],
                         dt.date(2000, 1, 1))
        self.assertEqual(response.context['form'].initial['tags'],
                         list(entry.tags.all()))
        self.assertEqual(response.context['form'].initial['locations'],
                         list(entry.locations.all()))
        self.assertEqual(response.context['form'].initial['content'],
                         'test content')


class DeleteEntryViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def setUp(self):
        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        entry = DiaryEntry.objects.create(
            owner=owner, title='test_title', date=dt.date(2000, 1, 1),
            content='test content',
            creation_time=make_aware(dt.datetime(2000, 1, 1, 21, 0, 0))
        )

        location = Location.objects.create(name='test_location', owner=owner)
        tag = Tag.objects.create(name='test_tag', owner=owner)

        owner.place_of_birth = location
        entry.tags.add(tag)
        entry.locations.add(location)

    def test_view_redirect_to_login(self):
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse(
            'diary:delete_entry',
            args=[owner.handle, dt.date(2000, 1, 1)])
        )
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get('/{}/{}/loeschen/'.format(owner.handle,
                                                             '2000-01-01'))
        self.assertRedirects(response, reverse('diary:entry_list'))

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse(
            'diary:delete_entry',
            args=[owner.handle, dt.date(2000, 1, 1)])
        )
        self.assertRedirects(response, reverse('diary:entry_list'))

    def test_show_404_if_entry_does_not_exist(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse(
            'diary:delete_entry',
            args=['vwxy', dt.date(1500, 1, 1)])
        )
        self.assertEqual(response.status_code, 404)

    def test_entry_is_deleted_from_database(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse(
            'diary:delete_entry',
            args=[owner.handle, dt.date(2000, 1, 1)])
        )
        self.assertRaisesMessage(
            DiaryEntry.DoesNotExist,
            'DiaryEntry matching query does not exist',
            DiaryEntry.objects.get, title='test_title',
        )
        self.assertRedirects(response, reverse('diary:entry_list'))


class EditLocationViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        Location.objects.create(
            owner=owner, name='test_location', latitude='12345',
            longitude='67890', postal_code='54321', street='test_street',
            house_number='12', city='test_city', description='test description'
        )

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:edit_location',
                                           args=[slugify('test_location')]))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/orte/{}/bearbeiten/'.format(
            slugify('test_location'))
        )
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:edit_location',
                                           args=[slugify('test_location')]))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:edit_location',
                                           args=[slugify('test_location')]))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'], LocationForm))

    def test_form_contains_prefilled_values(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:edit_location',
                                           args=[slugify('test_location')]))
        self.assertEqual(response.context['form'].initial['name'],
                         'test_location')
        self.assertEqual(response.context['form'].initial['latitude'],
                         '12345')
        self.assertEqual(response.context['form'].initial['longitude'],
                         '67890')
        self.assertEqual(response.context['form'].initial['postal_code'],
                         '54321')
        self.assertEqual(response.context['form'].initial['street'],
                         'test_street')
        self.assertEqual(response.context['form'].initial['house_number'],
                         '12')
        self.assertEqual(response.context['form'].initial['city'],
                         'test_city')
        self.assertEqual(response.context['form'].initial['description'],
                         'test description')


class DeleteLocationViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def setUp(self):
        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        Location.objects.create(name='test_location', owner=owner)

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:delete_location',
                                           args=['test_location']))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/orte/test_location/loeschen/')
        self.assertRedirects(response, reverse('diary:location_list'))

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:delete_location',
                                           args=['test_location']))
        self.assertRedirects(response, reverse('diary:location_list'))

    def test_show_404_if_location_does_not_exist(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:delete_location',
                                           args=['non_existing_location']))
        self.assertEqual(response.status_code, 404)

    def test_location_is_deleted_from_database(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:delete_location',
                                           args=['test_location']))
        self.assertRaisesMessage(
            Location.DoesNotExist, 'Location matching query does not exist',
            Location.objects.get, name='test_location',
        )
        self.assertRedirects(response, reverse('diary:location_list'))


class EditTagViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        Tag.objects.create(owner=owner, name='test_tag', type_of_tag='event',
                           description='test description')

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:edit_tag',
                                           args=['test_tag']))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/tags/test_tag/bearbeiten/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:edit_tag',
                                           args=['test_tag']))
        self.assertEqual(response.status_code, 200)

    def test_view_contains_correct_form(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:edit_tag',
                                           args=['test_tag']))
        self.assertTrue('form' in response.context)
        self.assertTrue(isinstance(response.context['form'], TagForm))

    def test_form_contains_prefilled_values(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:edit_tag',
                                           args=['test_tag']))
        self.assertEqual(response.context['form'].initial['name'],
                         'test_tag')
        self.assertEqual(response.context['form'].initial['type_of_tag'],
                         'event')
        self.assertEqual(response.context['form'].initial['description'],
                         'test description')


class DeleteTagViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def setUp(self):
        owner = DiaryOwner.objects.create(
            first_name='test_owner_fn', middle_name='test_owner_mn',
            last_name='test_owner_sn', birthday=dt.date(2000, 1, 1),
            time_of_birth=dt.time(20, 15, 0)
        )
        Tag.objects.create(name='test_tag', owner=owner)

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:delete_tag',
                                           args=['test_tag']))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/tags/test_tag/loeschen/')
        self.assertRedirects(response, reverse('diary:tag_list'))

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:delete_tag',
                                           args=['test_tag']))
        self.assertRedirects(response, reverse('diary:tag_list'))

    def test_show_404_if_tag_does_not_exist(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:delete_tag',
                                           args=['non_existing_tag']))
        self.assertEqual(response.status_code, 404)

    def test_tag_is_deleted_from_database(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:delete_tag',
                                           args=['test_tag']))
        self.assertRaisesMessage(
            Tag.DoesNotExist, 'Tag matching query does not exist',
            Tag.objects.get, name='test_tag',
        )
        self.assertRedirects(response, reverse('diary:tag_list'))


class OwnerListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:owner_list'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/besitzer/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:owner_list'))
        self.assertEqual(response.status_code, 200)

    def test_owners_empty_if_none_in_database(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:owner_list'))
        self.assertTrue('owners' in response.context)
        self.assertTrue(len(response.context['owners']) == 0)

    def test_owners_not_empty_if_some_in_database(self):
        DiaryOwner.objects.create(first_name='test_owner_fn',
                                  last_name='test_owner_sn',
                                  birthday=dt.date(2000, 1, 1),)

        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:owner_list'))
        self.assertTrue('owners' in response.context)
        self.assertTrue(len(response.context['owners']) == 1)


class EntryListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:entry_list'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/eintraege/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:entry_list'))
        self.assertEqual(response.status_code, 200)

    def test_entries_empty_if_none_in_database(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:entry_list'))
        self.assertTrue('entries' in response.context)
        self.assertTrue(len(response.context['entries']) == 0)

    def test_entries_not_empty_if_some_in_database(self):
        owner = DiaryOwner.objects.create(first_name='test_owner_fn',
                                          last_name='test_owner_sn',
                                          birthday=dt.date(2000, 1, 1),)
        DiaryEntry.objects.create(
            owner=owner, title='title',
            content='content', date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime(2000, 1, 1, 20, 15, 0))
        )

        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:entry_list'))
        self.assertTrue('entries' in response.context)
        self.assertTrue(len(response.context['entries']) == 1)


class LocationListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')
        DiaryOwner.objects.create(first_name='test_name_fn',
                                  last_name='test_name_sn',
                                  birthday=dt.date(2000, 1, 1))

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:location_list'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/orte/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:location_list'))
        self.assertEqual(response.status_code, 200)

    def test_locations_empty_if_none_in_database(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:location_list'))
        self.assertTrue('locations' in response.context)
        self.assertTrue(len(response.context['locations']) == 0)

    def test_locations_not_empty_if_some_in_database(self):
        owner = DiaryOwner.objects.get(first_name='test_name_fn')
        Location.objects.create(name='test_location', owner=owner)

        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:location_list'))
        self.assertTrue('locations' in response.context)
        self.assertTrue(len(response.context['locations']) == 1)


class TagListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')
        DiaryOwner.objects.create(first_name='test_name_fn',
                                  last_name='test_name_sn',
                                  birthday=dt.date(2000, 1, 1))

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:tag_list'))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/tags/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:tag_list'))
        self.assertEqual(response.status_code, 200)

    def test_tags_empty_if_none_in_database(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:tag_list'))
        self.assertTrue('tags' in response.context)
        self.assertTrue(len(response.context['tags']) == 0)

    def test_tags_not_empty_if_some_in_database(self):
        owner = DiaryOwner.objects.get(first_name='test_name_fn')
        Tag.objects.create(name='test_tag', owner=owner)

        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:tag_list'))
        self.assertTrue('tags' in response.context)
        self.assertTrue(len(response.context['tags']) == 1)


class OwnerDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        DiaryOwner.objects.create(first_name='test_owner_fn',
                                  last_name='test_owner_sn',
                                  birthday=dt.date(2000, 1, 1),)

    def test_view_redirect_to_login(self):
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse('diary:owner_detail',
                                           args=[owner.handle]))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get('/besitzer/{}/'.format(owner.handle))
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse('diary:owner_detail',
                                           args=[owner.handle]))
        self.assertEqual(response.status_code, 200)

    def test_owner_not_empty(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse('diary:owner_detail',
                                           args=[owner.handle]))
        self.assertTrue('owner' in response.context)


class EntryDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        owner = DiaryOwner.objects.create(first_name='test_owner_fn',
                                          last_name='test_owner_sn',
                                          birthday=dt.date(2000, 1, 1),)
        DiaryEntry.objects.create(
            owner=owner, title='title',
            content='content', date=dt.date(2000, 1, 1),
            creation_time=make_aware(dt.datetime(2000, 1, 1, 20, 15, 0))
        )

    def test_view_redirect_to_login(self):
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse(
            'diary:entry_detail',
            args=[owner.handle, dt.date(2000, 1, 1)])
        )
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get('/{}/{}/'.format(owner.handle,
                                                    dt.date(2000, 1, 1)))
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse(
            'diary:entry_detail',
            args=[owner.handle, dt.date(2000, 1, 1)])
        )
        self.assertEqual(response.status_code, 200)

    def test_entry_not_empty(self):
        self.client.login(username='test_user', password='test_password')
        owner = DiaryOwner.objects.get(first_name='test_owner_fn')
        response = self.client.get(reverse(
            'diary:entry_detail',
            args=[owner.handle, dt.date(2000, 1, 1)])
        )
        self.assertTrue('entry' in response.context)


class LocationDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        owner = DiaryOwner.objects.create(first_name='test_owner_fn',
                                          last_name='test_owner_sn',
                                          birthday=dt.date(2000, 1, 1),)
        Location.objects.create(name='test_location', owner=owner)

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:location_detail',
                                           args=['test_location']))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/orte/{}/'
                                   .format(slugify('test_location')))
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:location_detail',
                                           args=['test_location']))
        self.assertEqual(response.status_code, 200)

    def test_location_not_empty(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:location_detail',
                                           args=['test_location']))
        self.assertTrue('location' in response.context)


class TagDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = get_user_model()
        user.objects.create_user('test_user', 'mail@test.com', 'test_password')

        owner = DiaryOwner.objects.create(first_name='test_owner_fn',
                                          last_name='test_owner_sn',
                                          birthday=dt.date(2000, 1, 1),)
        Tag.objects.create(name='test_tag', owner=owner)

    def test_view_redirect_to_login(self):
        response = self.client.get(reverse('diary:tag_detail',
                                           args=['test_tag']))
        self.assertEqual(response.status_code, 302)

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get('/tags/test_tag/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:tag_detail',
                                           args=['test_tag']))
        self.assertEqual(response.status_code, 200)

    def test_tag_not_empty(self):
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(reverse('diary:tag_detail',
                                           args=['test_tag']))
        self.assertTrue('tag' in response.context)
