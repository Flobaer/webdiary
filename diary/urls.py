from django.urls import path, register_converter

from . import converters, views


app_name = 'diary'

register_converter(converters.ISO8601DateConverter, 'date')
register_converter(converters.DiaryOwnerConverter, 'owner')

urlpatterns = [
    # views for creation
    path('neues_tagebuch/', views.new_diary_view, name='new_diary'),
    path('neuer_eintrag/', views.new_entry_view, name='new_entry'),
    path('neuer_ort/', views.new_location_view, name='new_location'),
    path('neues_tag/', views.new_tag_view, name='new_tag'),

    # views for diary (owner)
    path('besitzer/', views.owner_list_view, name='owner_list'),
    path('besitzer/<owner:handle>/', views.owner_detail_view,
         name='owner_detail'),
    path('besitzer/<owner:handle>/bearbeiten/', views.edit_owner_view,
         name='edit_owner'),
    path('besitzer/<owner:handle>/loeschen/', views.delete_owner_view,
         name='delete_owner'),

    # views for diary entryies
    path('eintraege/', views.entry_list_view, name='entry_list'),
    path('<owner:handle>/<date:date>/', views.entry_detail_view,
         name='entry_detail'),
    path('<owner:handle>/<date:date>/bearbeiten/', views.edit_entry_view,
         name='edit_entry'),
    path('<owner:handle>/<date:date>/loeschen/', views.delete_entry_view,
         name='delete_entry'),

    # views for locations
    path('orte/', views.location_list_view, name='location_list'),
    path('orte/<slug:slug>/', views.location_detail_view,
         name='location_detail'),
    path('orte/<slug:slug>/bearbeiten/', views.edit_location_view,
         name='edit_location'),
    path('orte/<str:name>/loeschen/', views.delete_location_view,
         name='delete_location'),

    # views for tags
    path('tags/', views.tag_list_view, name='tag_list'),
    path('tags/<str:name>/', views.tag_detail_view, name='tag_detail'),
    path('tags/<str:name>/bearbeiten/', views.edit_tag_view,
         name='edit_tag'),
    path('tags/<str:name>/loeschen/', views.delete_tag_view,
         name='delete_tag'),

    path('', views.recent_changes_view, name='index'),
]
