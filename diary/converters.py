import datetime


class ISO8601DateConverter:
    regex = '[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}'

    def to_python(self, value):
        date = datetime.datetime.strptime(value, '%Y-%m-%d').date()

        return date

    def to_url(self, value):
        return value.strftime('%Y-%m-%d')


class DiaryOwnerConverter:
    regex = '[134567890cfghjklrvwxy]{4}'

    def to_python(self, value):
        return value

    def to_url(self, value):
        return value
