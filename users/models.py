from django.contrib.auth import models


class UserManager(models.UserManager):
    pass


class User(models.AbstractUser):
    objects = UserManager()
